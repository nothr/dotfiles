contactsatyaki@gmail.com

# Communication Model

- **Source** - Generates data
- **Transmitter** - Data to signal
- **Transmission System** - Carries data from source to destination
- **Receiver** - signal to data
- **Destination** - Takes data
---

# Data Flow

## Simplex
- One way street -> Data travels in *only* one direction. 
## Half-Duplex
- Two way but one can speak at a time
## Full Duplex
- Two way and both can speak at one
![[Pasted image 20230819223750.png]]
---

# Commutation Tasks

- This refers to the process involving in transmitting, receiving, and managing data b/w devices and systems.
- These tasks are essential for ensuring reliable, efficient, and secure data exchange in various networking environments
## Data Encoding and Modulation
- The digital data is encoded into analog signals(*suitable  format*), often modulation techniques like **AM**, **FM**, **PM**
## Data Transmission
- The encoded data is transmitted over a communication medium such as wired and wireless
- The goal is to reach receiver with minimal degradation or interference
## Signal Multiplexing
- To send multiple data stream it uses multiplexing techniques like Time-division multiplexing (**TDM**) and frequency-division multiplexing (**FDM**)
## Error Detection and Correction
- During transmission the data can be corrupted due to noise. Some mechanisms like checksum and cyclic redundancy checks (CRC) are used to identify errors in the received data.
## Data Framing
- Data is transmitting data in chunks and frames to provide structure and sync
- Frames invokes adding special markers or headers to indicate *Beginning* and *End* of each frame
## Flow Control
- Regulates the rate of data at which it is translated. Ensures that the receiving device can handle the incoming data
- To prevent data loss
## Routing and switching
- In larger networks router help in forwarding packets based on network address
- Helps in optimizing path of the transmission medium.
## Addressing and Packetization
- The data is encapsulated in packets and frames which contains source and destination address.
## Protocols and Standards
- The communication tasks rely on protocols and standard rules for data exchange. Ex: `TCP`,`IP`,`Ethernet`,`WIFI`
## Data Reception and Decoding
- The receiver decodes analog to digital and processed if necessary of corrections are required
## Data Demultiplexing
- The receiver demultiplexing the combined steam to individual streams
## Data Delivery and Acknowledgment
- Once the data is received it sends an acknowledgement to the sender to confirm.
---

# Data Communication model
![[Pasted image 20230819233406.png]]---
# Transmission of Information
- Based on requirement and Budget transmission servers are mostly costly component of a communication budget.
- Maximize the amount of information that can be carried over or to minimize the transmission capacity needed to satisfy a given information .
- **Multiplexing** and **Compression** are major approaches for *greater efficiency*
- **Multiplexing** -> Ability of no of devices that can share the transmission medium.
- **Compression** -> Squeezing data so that we can use a cheaper transmission facility.
# Types of connections
- Only one transmitter in both connections
- **Point-to-Point** -> Provides a dedicated link b/w two devices and the link is only *reserved for them*
- **Multi-point** -> Two or more devices use a specific link and It is a time shared connections
![[Pasted image 20230819234942.png]]---
# Topology
- **Network** -> A set of devices(nodes) connected by communication links
- **Physical Topology** -> The connection links b/w two or more devices is called topology.
## Mesh Topology
- Every device has a dedicated point-to-point link to every other device.
## Star Topology
- Each device has a dedicated point-to-point link only to a central controller, called a hub. 
## Bus Topology
- **Bus** -> The main cable is called bus
- **Drop Line** -> The line b/w *bus* and *device*
- **Tap** -> Connects the Drop lane and main cable
![[Pasted image 20230819235841.png]]
## Ring Topology
- The devices are connected in a circle
# Networks
- **Node** -> A device which is capable of sending and receiving data
## LAN (Local Area Network)
- Covers a small space like a office or a campus
- 10 Mbps to 100 Mbps (Very speed) 
## WAN (Wide Area Network)
- A group of MAN and large distance network
- Covers a long distance
## MAN (Metropolitan Area Network)
- Connection of LAN's in the same geographical area
## PAN (Personal Area Network)
- A basic home network with printers and telephone

---
# WAN Tech
## Circuit Switching
- A dedicated path is reserved b/w two stations using nodes
- Path must be reserved in Advance. Ex: Telephone network.
## Packet Switching
- Without dedicated network the data is sent in the form of packets
- Sent from one node to other node leading from source and destination.
- At each node the packet is received and sent to next node. Ex: Computer to Computer connection
## Frame Relay
- To compensate loss these packets have big headers
- Errors can be caught in end system
- This provides higher speeds with most error controls overhead removed.
## Async Transfer Mode
- Uses fixed-length packets called **cells**
- Provides little overhead for error control. Designed to work at high speeds
---
# The Internet
- Evolved from Advanced Research Projects Agency(ARPANET) by US defense department
- First **Operational packet-switching network**
- Led to Standardized TCP/IP protocols
## Internet Elements
- The purpose of internet to interconnect end systems called hosts
- A host can send data to another host via *Ip packets* and *Ip datagrams*
- Each packet has a IP address and travels through the series of router
## Internet Architecture
- Bunch of LAN's connected to the ISP(*Internet Service proivder*) through a POP(*Point of service*)
- **POP** -> A site that has a collection of Telecommunication equipment basically auth's user for acessing
- **Network Access Point** -> Physcial facility that provides infrastructure to move data between connected networks.
- **Network Service Provider (NSP)** -> Provides backbone serves to ISP
![[Pasted image 20230820132627.png]]---
# Protocol Architecture
- is a layered structure of hardware and software that supports exchange of data b/w systems
## Key Elements of a Protocol
- **Syntax** -> concerns format of the data
- **Semantics** -> Includes control info for coordination and error handling
- **Timing** -> Includes speed matching and sequencing
---
# TCP/IP protocol
- Applications  -> Computers -> Network
## TCP/IP Layers
## Physical Layer
- Covers the physical interface b/w hosts
	### Issues
	- Data rates
	- The nature of signals
	- The characteristics of transmission medium
## Network Access Layer
- Concerns with exchange of data b/w *Network* and *Host*
- Destination address provision
## Internet Layer
- Provides procedures to allow data to traverse multiple networks
- **Internet Protocol(IP)** -> is used at this layer to provide routing function across multiple networks
- IP not in end system buy in routes
## Host to Host or Transport layer (TCP)
- TCP is most commonly used
- Collects mechanisms in a common layer shared by all applications to provide reliable delivery of data
## Application Layer
- Contains logic needed to support various user applications
## Addressing requirements
- Each host must contain a global network address (IP Address)
- Each app must have unique address (PORT)
- TCP take data and sends to IP to forward the DATA
# Operations of TCP/IP
- IP header has control information
- TCP 32 bit 20 octets
- UDP 32 bit 8 octates but no guarrentee
- IP 32 bit 20 octates
# OSI
- Encapsulating down to up
- Decapulating Up to down
- Appplication make request but services recive them